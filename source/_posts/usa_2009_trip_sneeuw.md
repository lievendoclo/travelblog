title: Een trip door de sneeuw
date: 2009/05/18
categories:
- USA 2009
---

Onze eerste drive day.

Vrij laat opgestaan, voor de eerste keer na negen uur uit ons bed, maar toch weer voor onze wekker. This is getting boring... Ons ontbijt iets dichter bij het hotel gevonden, bij Peter D's. Opnieuw op en top Amerikaans, onze dienster noemde Rosie en de koffie vloeide rijkelijk. Farrah opnieuw gekozen voor de stack of pancakes met worstjes, ik de #2 (2 pancakes met bosbessen en 2 eieren met spek). Snelle bediening (hier kunnen ze nog wat van leren in België), je betaalt 1 koffie en je kop blijft nooit leeg. Lekker, maar zoals we ondertussen al gewoon worden, veel te veel.

Terug naar het hotel, koffers gepakt en ik achter de auto. Een wandeling van 20 minuten langs een 10-tal blokken. Even moeten wachten en uiteindelijk weggereden met een PT Cruiser, een gratis upgrade :). Spijtig genoeg maar 1 chauffeur, voor meer chauffeurs moest een surcharge betaald worden... Mijn eerste ontmoeting met 1. een automatische versnelling en 2. het hectische verkeer. 10 minuten later in het hotel geraakt (don't ask) en alles in de auto gegooid.<!--more-->

So we hit the road. Over de Bay Bridge en langs de Amerikaanse highways naar onze eindbestemming, Mammoth Lakes. Maar wat tussenin gebeurde, is het interessante gedeelte, wat we jullie niet willen laten missen.

Vanuit ons reisbureau, Verbeke Reizen, hadden we een volledige routebeschrijving meegekregen. In het begin ging alles goed, maar ergens hadden we een verkeerde afslag genomen (tis te zeggen, niet verkeerd, want we volgden het plan). In Amerika zijn er soms 3 afslagen die hetzelfde noemen. En wij dus de eerste afslag genomen. Verkeerde afslag, want waar we rechts moesten afslaan, stond onze bestemming naar links. Ok, wij dus naar links. Doorheen enkele typische cities even gestopt bij een food store: we hadden dorst en kregen de raad om een isomo box te kopen, te vullen met ijs (gratis overal te krijgen) en daar onze drank in te bewaren. Die gaan we morgen nodig hebben als we door Death Valley moeten... Aan de kerel achter de toog gevraagd hoe we moesten rijden, en whadda ya know, we zaten op de juiste weg. Farrah en Lieven: 1, Google Maps: 0! Uiteindelijk opnieuw op onze route geraakt die we moesten hebben.

Man man, wat een zicht langs de baan. Je kan het je echt niet voorstellen. Je gaat eerst door het heuvelland van San Francisco en omstreken, dan alles plat en dan... sneeuw. Frickin' snow! Onze originele route ging door Yosemite, maar de Tioga Pass was nog steeds gesloten. Gelukkig was onze beste alternatief, Route 108, wel open.

Je start langs wat je kan vergelijken met een trip door de Ardennen. Veel bos, veel bomen... Maar wat volgt kan je met niks vergelijken. De wegen worden bochtiger en alles gaat omhoog. Langs de route 6 squirrels tegengekomen (zonder wifi) en 1 hert. Die squirrels hebben denk ik opleiding gehad als zelfmoordterroristen, want elke keer liepen die in de richting van de auto. De eerste keer geremd, de volgende moesten hun geluk maar beproeven. De eerste ging all the way, de tweede  durfde maar halfweg, de derde zong een klaaglied bij een gesneuvelde vriend... you get the idea. De laatste zat halverwege de baan en in plaats van de juiste richting uit te spurten ging die richting mijn banden. Geen nood, geen gewonden, maar die gaat wel een nieuwe pamper nodig hebben. Gewoon al de glooiingen in het wegdek maken van deze rit een geweldige ervaring. Dit is een droom van elke motard!

Dan moesten we Sonora Pass door. Die was nog maar een week open en dat zullen we geweten hebben. Je begint met een helling van 25% omhoog langs kronkelige wegen. Langs de baan een pick-up tegengekomen met een trailer om U tegen te zeggen. Die gaat afzien, dacht ik bij mezelf. Hij heeft me dan ook vriendelijk voorbijgelaten :). Dan begint de sneeuw en de regen.
En sneeuw hebben we gezien. Een muur langs de baan, soms 2 meter... Het zicht wanneer je door die sneeuw rijdt is gewoonweg adembenemend. Woorden kunnen echt niet omschrijven wat je daar ziet.

Spijtig genoeg, everything that goes up must come down. En het was gewoon de beklimming in omgekeerde richting. Ooit al eens een helling met sneeuw langs de zijkanten van 25% naar beneden gereden? Je moet het eens doen. Die gast met zijn trailer gaat peentjes gescheten hebben, ik ben het zeker. Met een vreemde auto dit doen... ik ga eerlijk zijn, er zijn momenten geweest waar ik toch mijn rijkunsten in twijfel trok. Aangezien je deze blogpost leest, we leven allebei nog.

Eens door de Pass krijg je weer een zicht op dorre grasvlakten met sneeuwtoppen langs de zijkant.
Om het nog eens volledig te maken passeer je dan ook nog eens langs Mono Lake, een meer met een grote M. Het zicht is geweldig.

In het totaal 8 uur en 280 mijl later gearriveerd in Mammoth Lakes. Ingecheckt in de kamer (twee bedden deze keer, onze bagage wil ook wel eens comfortabel slapen newaar) van waar ik jullie nu dit bericht schrijf. Op naar de volgende rit: Death Valley! Moehaha!
