title: Onze laatste blogpost
date: 2009/05/29
categories:
- USA 2009
---

Ja, aan alle mooie liedjes komt een einde, en ook aan deze reis.

*Snif, ik wil niet naar huis... No offence, mama en papa maar ik wil meer van dit moois zien hier in Amerika.*

Vandaag een vrij korte post, aangezien we ook niet zo veel gedaan hebben vandaag. Deze morgen een ontbijt genomen in de refter (wegens gemakzucht en de ontdekking dat je daar wel internet hebt). Het brood dat we gisteren gekocht hadden was al zuur geworden (goe spul mannen), dus een breakfast burrito voor den dezen, en worstjes voor de wederhelft.<!--more-->

*Internet, lees: onze blogposten opladen om onze ongeruste ouders gerust te stellen...*
*En mijn neus in mijn eten steken, heb ik geleerd van mijne pa dus: dank u wel papa of ik zat wss lange tijd op het toilet...*
*Niet enkel worstjes... ook enkele van mijn schat zn rozemarijnpatatjes gepikt... (ik moest toch iets hartigs binnen hebben?)*

Alles de auto in en op weg naar onze laatste rustplaats (klinkt serieus, niet?). Onder de baan kwamen we wel nog de Imax bioscoop tegen en daar hebben we genoten van enorm mooie presentatie over de Grand Canyon. Toch wel een aanrader. En dan verder naar Phoenix.

Onderweg even gestopt voor de benen te strekken, maar dat is niet zo gemakkelijk. Phoenix is een warm klimaat en dus ideaal gebied voor slangen en schorpioenen. Uiteraard niet de lievelingsbeestjes van Farrah. De stop was dus van korte duur.

*Toch voor mij. Ons Lieven liep daar maar op die paden tussen de gebieden die ideaal zijn voor die beesten. Ikke daar maar achter gedrenteld, maar toch niet van harte hoor... brrrrr.
Ik ben zo vlug mogelijk weer in de auto gekropen, Lieven heeft dan nog even het kleine kamertje bezocht, ook op z'n dooie gemakken... Da is er om vragen he, om één van die beesten tegen te komen! Maar gelukkig is mijn schat heelhuids in de auto geraakt, gelukkig maar.*

On a side note: ik heb echt een hekel aan het rijden in een grootstad. Als je gewoon bent van op rustige wegen te cruisen, een stad zoals Phoenix of Las Vegas is stresserend...

*Stress? Stress? Ooit een vrouw gezien die haar vriend aant uitrazen is (mij niet meegeteld)? Imagine a man doing that... Very... euhm... peculiar, laten we het zo zeggen. Ik heb echter wel een wijze les geleerd: ik weet nu wanneer ik mn klep moet houden. Ik was er zeker van als ik mijn bijdrage had geleverd, het er niet veel goed aan gedaan zou hebben. En wat hebben we geleerd vandaag???*

Eens aangekomen in ons hotel ('t is van naam veranderd, 't noemt nu Xona) allebei gretig gebruik gemaakt van het zwembad. We hebben dat wel verdiend...

Dus hiermee is ons blog tot een einde gekomen. We willen toch enkele mensen bedanken.

*Op Nicci French's wijze  ;-)*

Line van Verbeke Reizen. Je routeplanning is onschatbaar gebleken. Echt bedankt voor je inzet.

Jenny van The House of Prime Rib. Echt waar, mensen zouden meer aan je bar moeten zitten. (Deze blog moeten we nog vertalen in het Engels...)

Al onze trouwe lezers. Het doet deugd om af en toe eens een reactie te horen van het thuisfront, hoe onnozel ook.

Iedereen die we vergeten zijn: bedankt!

Farrah en Lieven... signing out. Tot in België.
