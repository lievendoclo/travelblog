title: Tweede dag in San Francisco
date: 2009/05/17
categories:
- USA 2009
---

Opnieuw voor de wekker opgestaan... Hopelijk wordt dit geen gewoonte. Deze keer toch later uit het bed geraakt. Deze keer niet gekozen voor de American Breakfast en samen een klein broodje gedeeld. Loving is sharing newaar.

Vandaag was de andere kant van San Francisco aan de beurt: de Marina, dichter bij de Golden Gate,... Opnieuw ferme miles in onze voeten met andere woorden. Zowel Farrah als ik deze keer gekozen voor de meer sportieve schoenen, gezien ons debacle van gisteren... Of het zou werken zouden we wel onderweg werken.<!--more-->

Wij dus naar de pier dicht bij Girardelli's, waar je een beter zicht hebt op Alcatraz en the Golden Gate Bridge. Alcatraz... jah... normaal gezien gingen we dus de night tour doen, omdat je op die manier meer ziet dan de day tour. Ik wou reeds in België de tickets bestellen, maar dit lukte niet. Geen nood dacht ik, het zou wel lukken als we er waren. Zoals je kan denken: niet dus. Night tour volzet en wij dus overleggen wat we zouden doen: 40+ dollar per persoon betalen voor de normale tour of uitstellen naar het volgende bezoek aan Frisco (komt er zeker, 2 dagen is echt te kort). Uiteindelijk gekozen voor de tweede optie.

Even terug naar Fisherman's Wharf, er zat stof op de lens van ons fototoestel. Wij dus terug naar de fotowinkel die we de dag eerder bezocht hadden (elk een polarisatiefilter gekocht aan een belachelijk lage prijs). Gisteren zei ik dus al dat er fotowinkels genoeg zijn in SF, en ze zijn allemaal erg uitgebreid. Farrah en ik kwijlden op de lenzen en ja hoor... den dezen heeft zich laten verleiden. Nu ja, niet voor een lens. Da's te duur... Maar wel een opzetstuk voor eender welke lens die breedbeeld voorziet.

Van daaruit vertrokken naar de Marina. Dit is ook een van de minder toeristische plaatsen, je ziet hier meer locals zonnen op het strand. Opnieuw een hete dag (27°+) en dus veel volk op het strand. Geen Miami beach-achtige toestanden met Baywatch babes, nee gewoon je standaard Amerikaan (opnieuw geen dikzakken, maar die ga je dan ook niet zo vaak op het strand zien newaar). Spijtig toch...

Eens je daar bent, sta je ook een stuk dichter bij de Golden Gate, dicht genoeg om wat betere foto's te trekken. Ook hadden we op de weg naar de Marina een gebouw gezien dat we toch eens van dichterbij wilden bekijken. Dit bleek dus het Palace of Fine Arts te zijn, een gebouw in een stijl die je eerder in steden als Rome zou zien dan in een stad als San Francisco. Nu zijn we beiden geen cultuurfreaks, maar het was toch echt wel de moeite.

Terug naar de Wharf, maar er was nu wel iets meer dan een zuchtje wind en dat zorgde er toch voor dat we allebei het iets kouder hadden dan we hadden gedacht. Eens op de Wharf wilde ik toch wel eens de Clam Chowder proberen. Farrah koos voor de ietwat conservatievere Shrimp Cocktail ( scampi's geserveerd dmv een martini-glas). Toegegeven, het was lekker, maar overrated. Een lekkere bouillabaisse smaakt voor mij toch net beter...

Op de weg naar de Marina kwamen we ook een van de natte dromen tegen van Farrah: een Barnes & Noble. Iedereen die Farrah kent weet dat ze boeken verslindt en dit is dan ook het manna voor mijn schat. Maar we hadden beiden koud en de voeten begonnen weer te protesteren, dus het bezoek was kort maar krachtig... zonder boeken.

Nu wilden we echt wel terug naar het hotel en ons plan was de Cable car te nemen. Nu, de rij voor die attractie was HUGE. Dan maar geopteerd voor de snelle oplossing en een taxi genomen. Heel gemakkelijk is dat hoor, gewoon langs de straat staan en je hand opsteken als er een passeert. In no time terug in het hotel.

Dan... eten. Aangezien onze fantastische ervaring met Jenny en Peter gisteren in het House of Prime Rib, wij terug naar daar voor een aperitief. Peter had verlof, Chris stond in zijn plek. Ze herkenden ons direct en wij terug aan de bar. Als ik iemand een tent moet aanraden voor cocktails, dit is het. Echt waar, die kennen hun stiel. We vroeger aan Jenny hoeveel ze er kende, she lost count, meer dan 100 in ieder geval. Elk begonnen met een Lemon Drop Martini. 3 cocktails later (remember, 1 bestellen is 3 krijgen) keken we naar elkaar en besloten om nog eentje te nemen. Jenny heeft iets klaargemaakt voor Farrah, ik bestelde de gevreesde Long Island Iced Tea. Ik zeg gevreesd, want de laatste keer dat ik dat bestelde (de Stupa in Brugge) kreeg ik een cola met rum. Toch maar eens die mannen naar hier sturen, want deze was super. Niet te zoet, meer dan genoeg drank ;). Nu eh... van eten is er niets meer gekomen, onze honger was gestild... kinda :). De eerste cocktail hebben we niet moeten betalen, die was on the house omdat we de dag erna vertrokken!

Licht in ons hoofd terug naar het hotel, de volgende nacht tegemoet. Ik spaar u de details.
