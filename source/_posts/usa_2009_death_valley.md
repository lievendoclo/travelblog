title: Death Valley, we have arrived
date: 2009/05/19
categories:
- USA 2009
---

Vandaag een tweeluik, aangezien we in Death Valley geen internetverbinding hadden (wat wil je...) wordt dit bericht net zoals het volgende in over-the-top Las Vegas geschreven.

Weer eens voor de wekker opgestaan. Troost jullie, tis de laatste keer dat ik dit zeg, we worden het ondertussen al gewoon. Gratis ontbijt gekregen omdat er werken aan de gang waren in het hotel. Het hotel trouwens viel ongelofelijk goed mee. Nog nooit meegemaakt dat de receptie belt om te vragen of alles ok is... Na ons ontbijt voor de eerste keer gaan tanken in de VS (tis ier al naft dat de klok slaat, diesel vervuilt te veel he). Eerst betalen, dan tanken en dan de rest terugvragen... Vrij raar, maar het zal wel wennen. <!--more-->

Mammoth Lakes is koud, toch naar wat we ondertussen al gewoon zijn, misschien een 15-tal graden. Eigenlijk wel een leuke stad als je het zo bekijkt. Tijdens de winter is het een hemel voor wintersporters (als ze er al geraken, that is), tijdens de zomer is het de occasionele doortrekker of visser die hier voor de toeristische inkomsten moet zorgen.

Wij dan maar op ons gemak naar Furnace Creek. Voorziene duur: 4u30m, maar aangezien we vaak gestopt hebben voor foto's is dat net iets langer geworden. De weg naar Death Valley is achteraf gezien vrij saai vanuit Mammoth, niks anders dan highway. Maar je gaat wel langs enkele typische Amerikaanse steden, waaronder blijkbaar eentje waar een rodeo werd gehouden. Spijtig genoeg geen tijd, dus wij al rijdend vlug een glimp opgevangen van de paarden.

Dan Death Valley in. Je moet eerst een volledige bergketen door (Death Valley is eigenlijk een gigantische put tussen bergketens) en ja hoor... regen. Vrij absurd eigenlijk, 30° met regen. Je bent nat, maar een minuut later zit je weer te puffen van de warmte. We krijgen een voorsmaakje van wat ons staat te wachten: een prachtig silhouet van bergen, sneeuw en hier en daar een struikje. Maar eens je over de Panamint keten zit, krijg je een ongelofelijk zicht op wat het begin van Death Valley is. Gewoon onbeschrijfelijk, je moet het met je eigen ogen gezien hebben om het te kunnen geloven. Ik kan het best omschrijven als onwereldlijks, een Mars-achtig landschap met Aardse tinten. Je gaat eigenlijk de hele tijd omlaag (Death Valley ligt meestal lager dan de zeespiegel).

Uiteraard, Death Valley is een nationaal park, dus dat wil zeggen: betalen... Volgens ons boekje 50 dollar per auto, maar de inflatie is hier ook toegeslaan: de prijs is nu 80 dollar. Gelukkig is hiermee ook onze toegang voor alle andere parken betaald (voor een volledig jaar, dus als er nog iemand dit jaar naar de VS gaat voor de nationale parken, je weet me te vinden, mits een kleine bijdrage uiteraard ;)).

Death Valley op zich is een samenraapsel van raar uitziende planten, maanlandschappen, zoutvlaktes en vooral: een verzengende hitte. Je kan het echt niet geloven hoe verdomd warm het hier is. Volgens de ranger was het nog relatief koud... Ja mijn botten zeg!

Langs de baan enkele keren gestopt, de eerste keer in Salt Creek, een kleine oase binnen Death Valley. Tijdens de winter komt het water vanuit de bergen in Death Valley gestroomd (ze hebben hier echte flash floods) en blijft op bepaalde plekken staan. Hier is er een soort van leerrijk pad rond gemaakt. Farrah, de anti-slangenmens dat ze is, is er uiteraard niet zo gerust op. In Death Valley zitten nu eenmaal slangen. Ze schoot dan ook een meter in de lucht toen er een kleine hagedis even langs het pad kwam. Ik denk dat dat beestje meer verschoten is van haar dan zij van hem... Terug naar de auto voor de volgende leg of the journey (20 min in die warmte en je vergaat van de dorst).

Langs de baan staan hier en daar borden van zaken die je moet gezien hebben, bijvoorbeeld Devil's Corn Field, een veld van wat van ver af een hoop overmaatse maiskolven in de woestijn zijn, of de Sand Dunes, de bergen zand die gans het jaar door van plaats veranderen... Een mens vraagt zich dan af of ze ook de plek van die borden veranderen :S. Omwille van de hoeveelheid zand dat hier rondwaait laat je best je ruit hier dicht...

Wat later dan aangekomen in Furnace Creek, een van de weinige dorpen in Death Valley. Ingecheckt, koffers op de kamer en even genieten van de airco. Ik had op weg naar de kamer een winkel gezien en het restaurant, dus ik even naar buiten voor wat inkopen te doen. Het restaurant, ja, tis echt er aan te zien dat ze het enige restaurant zijn in omstreken: belachelijk hoge prijzen. Onze lunch die avond is dan ook geworden: hotdogs met brood en ketchup. Véél goedkoper en ongetwijfeld even lekker. Direct ook pindakaas en confituur mee en we hadden ons ontbijt voor de dag erna (ook belachelijk duur...).

Aan het hotel was ook een horse ranch gekoppeld, maar de viervoeters waren ofwel verdampt, weggelopen of in het restaurant beland, want ze waren in ieder geval niet te zien. Farrah uiteraard beetje teleurgesteld...

's Avonds laat nog naar de originele versie van Sterren op de Dansvloer gekeken (veel meer entertaining dan onze versie, da zeker :)) en op ons gemak in slaap gevallen...
