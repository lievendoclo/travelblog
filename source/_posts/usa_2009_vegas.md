title: Op naar Vegas
date: 2009/05/20
categories:
- USA 2009
---

Slapen in Furnace Creek is niet zo evident. De temperaturen gaan hier niet lager dan 27° en zelfs met airco is het niet zo evident om een deftige temperatuur te vinden op de kamer.

Geen standaard onbijt in het hotel, maar eerder de rest van ons brood met melk, pindakaas en confituur. Het ontbijt is hier echt niet te betalen, dus deze oplossing is wel niet zo slecht. Misschien doen we dit wel voor de rest van onze reis.

Uitgecheckt en de auto in op weg naar Las Vegas. Er is een heleboel Death Valley nog te zien, dus onze voorziene rit van een 2u is wat uitgelopen. Eerst naar de Badwater Basin, het laagste punt in de VS (en het westelijk halfrond for that matter). Geloof me, je kan je echt niet inbeelden hoe verdomd warm het is. Zet 30 seconden de airco af en de auto wordt een sauna... Langs de weg naar Badwater kom je enkele paden tegen, maar deze keer gekozen om ze niet te nemen. We willen onze auto graag in 1 stuk teruggeven en de paden hier zijn allesbehalve verhard. Het lijkt soms dat je in een rijdende vibrator zit...<!--more-->

Uiteindelijk kregen we Badwater in het vizier. Een grote zoutvlakte met een grote parking... Zoals alle bezienswaardigheden hier kan dit er niet aan ontsnappen. Op de weg naar de parking komen we een fietser tegen. Een fietser! Furnace Creek is 20km verder weg, die kerel moet zeker een zonneslag gehad hebben voor hij vertrok.

Geparkeerd, een halve fles Sprite leeggedronken en een wandeling op Badwater Basin gemaakt. Na 5 minuten waren we allebei gaar en teruggekeerd naar de auto. Onderweg de fietser tegengekomen die in de schaduw aan het afkoelen was. Maar of dat lukt met 40+° in de schaduw, ik weet het niet. Maar hij mag ook niet te veel afkoelen, anders beseft hij dat hij nog terug moet...

Op de weg terug naar de highway de Artist's Drive gedaan, een eenwegsbaan die je langs de ongelofelijk prachtige heuvels van Death Valley stuurt. Onderweg stop je even aan Artist's Pallette, een stuk heuvels met geweldige kleurschakeringen. Prachtig gewoon.

Op naar Las Vegas. Vrij gemakkelijke baan om te rijden, straightforward. Maar zoals gewoonlijk met ons, niks gaat zo gemakkelijk als het lijkt. Toen we vanuit Furnace Creek vertrokken hadden we nog vrij veel benzine in de tank. Maar 30 mijl van Las Vegas schiet het lichtje van de benzinetank aan... SHIT! En aangezien het bereik sneller slinkte dan het afstand die we reden (Chrysler, indien jullie dit lezen, DIT IS NIET LEUK) begonnen we onze keuzes te bekijken. Terugkeren was geen optie, Vegas was dichter dan de stad die we eerder gepasseerd hadden. Gelukkig ging het vanaf nu omlaag, dus hebben we datgene gedaan dat je NOOIT mag doen: de auto in N zetten en laten bollen... It worked, we bereikten zelfs het tankstation met nog voor 43 mijl in de tank. Lieven en Farrah: 1, Chrysler: 0.

Into Vegas we go. Langs Las Vegad Boulevard gereden (die voor een deel The Strip genoemd wordt) en uiteraard de verkeerde afslag genomen. We zaten op de oprit voor de Bellagio. Verkeerd hotel, dus wij er weer af. Uiteindelijk in 1 stuk toegekomen in het Rio.

De parking op, sleutels ophalen, bagage ophalen en op naar de kamer. De kamer is groot. Echt groot. we hebben een living, een king-size bed, 2 wastafels (in een aparte kamer), een doorloopkast en een badkamer. Nu, ik las dat in de Venetian alle kamers 2 verdiepingen hadden, dus dit is niks :). Dan even het casino in. Kapitalisme ten top, je moet dat gewoon eens gezien hebben. Knipperende lichten overal, irriterende pling-geluiden,... Je kan het voor zo'n 20 min verdragen, daarna moet je echt buiten...

Maar we begonnen allebei honger te krijgen, dus dan maar naar het all-you-can-eat buffet in ons hotel. Een seafood-buffet dan nog wel! Nu ja, zoveel eten doen we toch niet, maar het was toch wel eens leuk om krabbenpoten, -scharen en andere soorten vis te verorberen. Na een bord of twee (en dessert, ondertussen weten we allebei hoe pinda-ijs smaakt) besloten we om naar de kamer te gaan en uit te rusten voor morgen.
