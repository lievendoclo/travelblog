title: Bryce Canyon...
date: 2009/05/23
categories:
- USA 2009
---

Vandaag eens een vroegere blogpost. Het is namelijk niet zo geweldig weer buiten: onze eerste dag echte regen.

Dus eens Bryce Canyon gedaan. Hoe onaards de Red Canyon al leek, Bryce lijkt zeker aangelegd te zijn door Marsmannetjes met een ziek gevoel voor humor. Puntige rotsen met hier en daar een spar tussen. Ook de kleuren van de canyon doen je eerder denken aan een Marslandschap dan aan iets hier in de VS.

Toen we toekwamen was het al een beetje aan het druppelen. Nu ja, een beetje regen houdt ons niet tegen. Maar aan het schoeisel te zien (voor zover je het nog kon zien) van de mensen die terugkwamen, stond ons nog blijkbaar wat te wachten. <!--more-->

Na de eerste bocht was het al prijs. Dikke, oranjerode modder waaraan geen einde bleek te komen. Na de eerste helling keken we naar elkaar en besloten we om toch maar terug te keren. Met afgronden van 100m naast het pad denk je wel eens twee keer na, niet waar? We hebben gelukkig wel enkele geweldige foto's kunnen maken.

Op de weg terug naar de auto, vond ik toch dat we er ons eens moesten aan wagen. Dus wij tweetjes het Navajo pad naar beneden, in de drop. Een zigzaggend pad naar beneden en ondertussen genieten van de haast onmogelijke rotsformaties die je tegenkomt. Hier wil je echt niet zijn als er een aardbeving is, de boel komt zeker naar beneden... Eens beneden kom je een hoop bomen tegen, die je niet kon zien van bovenaf. Ze zitten werkelijk tussen de rotsen in en blijken het wel leuk te vinden. Ieder zijn meug, niet waar.

Zo passeer je langs Wall Street, niet genoemd naar het kapitalistisch bastion in New York, maar omdat je gewoon tussen twee rotswanden waar geen einde aan komt lijkt te lopen. Spijtig genoeg moet je weer naar boven en op dat moment brak er toch een straaltje zon door de wolken. Als het al niet erg genoeg was om door de modder naar beneden te moeten, probeer dan maar eens hetzelfde omhoog, maar dan met wat hogere temperaturen op je kop. Needless to say, het heeft toch iets langer geduurd om boven te komen. Onderweg een koppel tegengekomen dat in de namiddag nog eens naar de Grand Canyon ging rijden. Dat is pas een drukke dag...

Eens boven zagen ons schoenen eruit alsof ze een waren geworden met de rotsen. Ze wogen in ieder geval toch zoveel als rotsblokken. Gelukkig wist dat koppel ons te zeggen dat er scrubbers beschikbaar waren en na enkele minuten zijn we er toch in geslaagd om ons schoenen toch nog redelijk te laten uitzien. Wij hadden botines aan, er waren mensen naar beneden gegaan in tennisschoenen en zelfs in teensloefen... Liever zij dan ik.

In het hotel enkele hotdogs en buns gaan halen en dat als middageten verorberd. Twas wel al 15u, maar kom. Even uitgerust en toch besloten om eens terug te gaan om de andere viewpoints te zien. We komen aan bij Bryce Point en hebben nog geen 3 foto's getrokken, of de hemelsluizen breken open. Terug naar de auto, morgen geven ze beter weer.

Dus is het nu 19u en zitten we reeds op de kamer. Een welverdiend bad staat me op te wachten en Farrah zit op een kwart van haar laatste boek (ze had er 3 mee...). Mss vanavond nog een kleine post over onze reis in het geheel, voor mensen die eens willen weten hoe we dat nu eigenlijk aangepakt hebben.
