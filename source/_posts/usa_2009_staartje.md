title: Het staartje aan de reis
date: 2009/06/30
categories:
- USA 2009
---

Ik ging dus eens een blogpost doen over hoe we die reis aangepakt hebben. Ze staat al een heel eind klaar, maar ik kom er nooit toe om ze te posten.

Ons initieel gedacht was om een zelfgeplande reis naar Thailand te doen. Dat plan is een beetje op een zijspoor geraakt door enerzijds het taalprobleem dat we daar zouden tegenkomen, en de prijs van de vlucht, die toch wel vrij hoog kon genoemd worden.

Gelukkig was het dan vakantiesalon in Antwerpen en samen met een stuk overheerlijke gedroogde ham zijn we buitengekomen met een nieuwe bestemming: de VS. Het was iets dat we allebei eens wilden doen en ons kennende, we laten er geen gras over groeien.<!--more-->

Eerst even gekeken naar de georganiseerde reizen. Allemaal wel leuk, je krijgt je route, auto, hotel en wat informatie and you're on your way. De enige downside daaraan is het prijskaartje. Zo'n vooraf georganiseerde reis kost stukken van mensen. Nu, kannibalen als we zijn, hebben we gewoon alle brochures doorlopen zo'n beetje een best-of gemaakt voor wat wij in gedachten hadden.

Daarna het web op, op zoek naar de hotels. Eens je je bestemming kent, is het een makkie om hotels te vinden via de vele sites die je op internet kan vinden. Alles eens opgeschreven en op naar het reisbureau.

Nu, een reisbureau ziet je altijd graag komen met concrete ideeen. Dat maakt het voor iedereen gemakkelijker. Een reisbureau kan trouwens meestal ook voordeliger hotels en vluchten boeken. Een van de reisbureaus die ik al meerdere malen gebruikt  heb is Verbeke Reizen, dus ik en mijn vriendin daar naartoe.

En dan is het gewoon een planningspelletje. Onder de weg is de route aangepast (een van de reisagentes was daar reeds geweest en maakte enkele suggesties), de datum van vertrek omdat we dan voordeliger in bepaalde steden zaten, ... Eerst was onze vlucht en de auto geboekt. Hotelkamers zijn zelden een probleem, overal kunnen geraken is het belangrijkste. En dan konden we spelen met de dagen waarop we in de steden zaten. Na een aantal offertes die over en weer gemaild werden uiteindelijk neergekomen bij de finale versie.

Voor de nieuwsgierigen, even een prijszetting geven:

- De vlucht was 417 euro per persoon (heen naar SF, terug van Phoenix)
- De huurwagen 340 euro (4 deurs compact)
- De hotelkamers 696 euro per persoon

Totaalprijs voor de reis: ongeveer 1300 euro per persoon. Ik heb al verhalen gehoord van mensen die heel wat meer betaald hebben. Een van de zaken waarmee je moet opletten met de georganiseerde reizen: de vlucht zit NIET in de prijs inbegrepen.

Hou er ook rekening mee dat je brandstof en eten niet in de prijs ligt. Voor brandstof mag je rekenen een 40 dollar per volle tank, waarmee je zo'n 300  a 400 mijl doet afhankelijk van de wagen. Dus als je prijs 2000 mijl is, reken toch tussen de 200 tot 300 dollar brandstofkosten. Eten, dat bepaal je zelf. Wil je elke dag Taco Bell, Wendy's of Burger King, zal je met 20 dollar per dag wel toekomen. Wij hebben 40 tot 50 dollar per dag gerekend en daarmee kwamen we toe.

Nog een tip: drinken. Koop een isomo box in een tankstation (of nog beter, in Walmart, veel betere boxen) en koop elke dag 2 zakken ijs voor je drank koel te houden (of als het hotel niet kijkt, gebruikt de ice dispenser voor kleinere hoeveelheden :)). Iets wat we vanaf nu ook gaan doen is gewoon 2 gallons water kopen en 1 doos gatorade poeder. Gatorade lest je dorst enorm en je gaat goedkoper uitkomen dan blikjes te kopen (en het is milieuvriendelijker). 2 gallon is iets meer dan 7 liter, dus daar kom je wel even mee toe.

Een raad trouwens voor de huurwagen: huur niet het duurste model. Meestal als je laat genoeg aankomt in het verhuurbureau krijg je een betere wagen mee (nooit een slechtere) omdat het type dat je gereserveerd hebt niet meer beschikbaar is. Zo zijn wij vertrokken met een auto die dubbel zo duur is als degene die we gereserveerd hadden! Ook een tip: wil je ruigere paden verkennen, neem eem jeep. Onverharde wegen met een gewone wagen zijn geen lachertje.

Mocht er iemand nog vragen hebben over onze reis, laat een reactie achter met je contactgegevens en ik geef je graag de informatie die je zoekt.
