title: Arches National Park
date: 2009/05/25
categories:
- USA 2009
---

Vandaag opnieuw een heel drukke agenda. Arches National Park staat op het menu en het is geen klein gebied.

Maar voordat we onze dag beginnen: eerst ontbijt. Spijtig genoeg is de dag niet zo vlot vertrokken. Den dezen moest zo nodig de sleutel van de kamer verliezen (Farrah had er ook een, maar toch). Auto onderste boven gekeerd (niet gemakkelijk, zo'n PT Cruiser weegt nogal), rugzakken onderste boven (goed excuus om eens kuis te houden), niks. Kamer uitgepluisd, niks. Ok, niks aan te doen. Farrah toch eens naar de balie om het verlies te melden (kost 5 dollar per sleutel), en what would you know, een barmhartige samaritaan heeft de sleutel gevonden en daar afgegeven. Farrah toch de sleutel daar gelaten, om verdere problemen te vermijden :).

Dan eindelijk op weg naar ons ontbijt naar de bekenste ontbijttent in de VS: Denny's. Volledig ontbijt besteld. Je kon 4 dingen bestellen, Farrah wou er maar 3. Die ober van zijn tak gemaakt omdat het er maar 3 zijn, en Farrah dan maar kwaad een portie bacon besteld. Die gast kreeg wss zijn fooi op basis van het aantal ingrediënten... Goeie ruil dan maar gemaakt: mijn portie sausages voor haar portie bacon. Iedereen happy :).<!--more-->

De rit naar Arches is vrij kort, het park ligt 2 mijl van Moab. Opnieuw onze kaart voor alle parken getoond, en we mogen gratis binnen. Geloof me, doe je meer dan 3 parken tijdens je reis, neem een annual pass. Weer een kronkelend baantje naar boven, de auto begint het al gewoon te worden. Je moet er toch verdorie je verstand bij houden... Je begint niet onnozel te doen met de auto wanneer er een afgrond van 100m naast je auto is.

Arches is een park met heel veel bogen (vandaar de naam). Ik ga niet het volledig proces uitleggen maar 't is samen te vatten in 1 woord: erosie. Je kan er ongelofelijk veel zien in dit park, de kaart die je krijgt aan de ingang is van onschatbare waarde. Er staan wel pijltjes, maar dit moet je wel plannen.

De plaatsen die we bezocht hebben zijn te veel om op te noemen, maar enkele zijn bijvoorbeeld Devil's Garden, Delicate Arch, Windows (zelfs hier is Microsoft aanwezig... pfff), Turret Arch... Een voor een allemaal wondertjes der natuur. Onder een van die bogen toch uitgerust, toch wel een speciaal gevoel om te zitten op een rots met tonnen steen boven je hoofd.

De laatste plek die we bezocht hebben was de Landscape Arch. Om die te bereiken moet je toch eerst een ferme wandeling maken, langs rotswanden van honderden meters hoog, door Devil's Garden. En ja, soms lijkt het wel dat je door een geschifte botanische tuin loopt. Maar that's me again..

Gisteren onweer, en vandaag stond dit ook op het menu. We hadden net onze foto's getrokken en donkere wolken pakten samen. Dit voorspelde niks goed. Terug naar de auto dus. Onder de weg kennis gemaakt met een koppel hagedissen, die mooi poseerden voor ons.

We waren net aan de auto, en het begon te regen. Nu had ik niet echt zin om in gietende regen die kronkelende baan weer naar beneden te rijden, dus wij zo vlug mogelijk weer naar de Visitor's Center aan het begin. Niet zo evident, Devil's Garden ligt aan de andere kant. Een kleine 20 min later terug aan de ingang van het park geraakt. Nog steeds dropregen. Hmmm...

Aangezien de lichte regen, eerst even naar de supermarkt voor wat inkopen. We waren net binnen en de hemel brak open. Een stortbui is hier echt geen lachertje. Terwijl wij onze inkopen doen, valt opeens de stroom uit in de supermarkt. Enkele seconden later weer licht. Ah ja, wss een blikseminslag, niet erg.

Wel erg. Op de weg terug naar het hotel, geen licht te bemerken, zelfs de verkeerslichten lieten het afweten. Trouwens, de Amerikanen hebben een geweldig systeem op kruispunten zonder lichten: first come, first serve. En iedereen houdt er zich aan. Spijtig genoeg, dit zou nooit lukken in België...  Let's face it, wij zijn rotzakken in het verkeer.

Eens op de kamer, hetzelfde hier. Wel eindelijk een verklaring: de bliksem heeft de transformator geraakt. Gevolg: heel Moab zonder stroom. Oh the irony... het Department of Energy heeft een project net naast Moab. Wss een verkeerd knopje? Natuurlijk had ons hotel noodverlichting, maar zoals gewoonlijk, niet op weg naar onze kamer. Op de tast onze deur gevonden. Gelukkig had ik mijn Petzel mee. Ik was opeens vrij populair :).

Vanavond geen restaurant, maar tapas genot op de kamer. We hebben een microgolf op de kamer en we maken er graag gebruik van. Dus terwijl Farrah aan het nagenieten is van haar aromatherapeutisch bad, zit ik hier weer te schrijven. Tot morgen zou ik zo zeggen...
