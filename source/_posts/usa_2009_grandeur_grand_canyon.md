title: Grandeur van de Grand Canyon
date: 2009/05/27
categories:
- USA 2009
---

Vandaag nemen we Nicci French als voorbeeld: we gaan eens samen schrijven (voor de leken onder ons, Nicci French is een koppel die samen super thrillerverhalen schrijven)
Misschien dat een vrouwelijke toets wel gewaardeerd wordt door ons publiek?

*Waarom moet ze er nu altijd boeken bijhalen…*

Vanmorgen elk apart wakker geworden, jammer, maar ook wel eens leuk om te merken dat je al je lakens nog op je hebt liggen.
Het ontbijt was voor de verandering eens inbegrepen in de prijs, niet dat we gierig zijn hoor…
Het was niet hoogstaand maar lekker en voldoende: een continental breakfast met toast, cornflakes in alle soorten en maten, bananenbrood (voor Lieven), gebakken omeletjes, hamburgers (ondanks de Burger King van gisteravond kon ik het niet laten *blush*), muffins, bagels, confituur, peanutbutter… en fruitsap, melk, koffie en thee. Meer moet dat niet zijn, toch?<!--more-->

*Een pomme moskovite mocht altijd, maar blijkbaar wordt dat hier niet geserveerd… Toch een minpuntje. Farrah kan spijtig genoeg nog steeds niet aanzien, mijn boterhammen met pindakaas en confituur… Erg lekker hoor, moet je zeker eens proberen.*

Tegen 10u30 zaten we in de auto en gingen we onderweg naar Monument Valley, waar we niet meer geraakt zijn gisteren.
Wij zijn er dus achter gekomen waarom Indianen in een reservaat zitten: kapitalistisch als ze zijn, kunnen ze dan een inkomprijs vragen – lees: geen gebruik van onze annual pass, grmbl… - maar zoals eerder gezegd, we zitten niet op ons geld.

*En zelfs de senioren moeten hier betalen. Alleen ettertjes onder de 9 jaar mogen blijkbaar gratis binnen, maar komaan, welke ouder met een krijsend kind gaat een nationaal park binnen?*

Op het eerste zicht ging alles goed, een mooie verharde weg, maar dat liedje was ook niet van lange duur. Voor we het goed en wel beseften zaten we op een kiezel/zandweg met de nodige putten en bulten. Vandaag waren wij dus niet Lieven en Farrah maar Mr. en Mrs. Wigglyhead.
Al bij al toch mooie dingen gezien: Elephant’s Butte (uitspreken als bjoet, volgens de Amerikanen, maar aangezien het om een dubbele t gaat, denk ik dat het als buttée moet uitgesproken worden, soit, dat terzijde), Camel’s Butte, Thunderbird Mesa, the Hub, the Three Sisters, … De foto’s bewijzen het.

*Uiteraard verzwijgt Farrah hier dat ze zich even ontpopte tot rally copiloot… Ok, de omgeving met het stof en zo zet wel de sfeer, maar als je opeens iemand naast je hoort roepen ‘ go for it’, dan moet je maar eens proberen je in te houden. Opnieuw de grenzen van onze PT Cruiser verlegd… De mensen voor ons weken zelfs uit (ik wil wel geloven dat het voor ons was, maar het uitzicht was er ook wel…)*

En ook hier weer de nodige fauna gezien: kleine mooie lizards, schitterende paarden, wilde honden (toch wijselijk niet te dichtbij gekomen), schapen (met een bel rond de nek) en koeien, again.

*Even over die lizard, die heeft ook ferm mogen spurten, een PT Cruiser tegen 20 mph op je zien afkomen, dat doet je wel eens een versnelling hoger schakelen als je de weg oversteekt… Troost jullie, geen slachtoffers gevallen (behalve de lizard zijn schone onderbroek).*

Op de nodige viewpoints stonden Navajo’s (vermoed ik toch) met ‘originele handgemaakte juwelen’. De discussie of dit nu de waarheid is, laten we even achterwege. Toch moet ik toegeven dat er een paar mooie dingen tussenzaten. We hebben echter ons geld gespaard.

Goed, terug naar Kayenta, aangezien dit op de weg ligt naar waar we naartoe moeten: de Grand Canyon.
Aan de splitsing voorbij Tuba City (nee, geen getoeter gehoord, ondanks wat je zou vermoeden) is het wel een beetje misgelopen. We moesten beiden ongelooflijk dringend naar het toilet dus hebben wij een beginnersfout gemaakt: uitkijken naar het eerste beste tankstation ipv de ogen op de kaart en de baan te houden. Gevolg: onze afslag gemist. Nu moet je weten, hier in Amerika mag je niet zomaar de dubbele gele lijn oversteken om te draaien dus moesten we noodgedwongen verder rijden naar het eerst volgende stadje om daar dan op een kruispunt terug te keren.
Tussen die afslag en dat kruispunt een verloren gelopen hond bijna een kopje kleiner gemaakt: stond die daar opeens, te midden van ons rijvak, van links naar rechts te kijken. Kon hij beter gedaan hebben voor hij op de weg stond als je het mij vraagt… Gelukkig hebben we die loebas nog tijdig kunnen ontwijken, dankzij het gebrek aan een achterligger.

*Ik maar toeteren en dat beest maar blijven staan. Ik ga naar rechts, en dat beest ook naar rechts. Doet me denken aan een eerder voorval…*

Uiteindelijk zijn we toch nog op de juiste baan geraakt. Wel even een verward momentje gehad toen ik (als navigator van dienst) Lieven stuurde in de richting waarvan hij zei aan het begin van de rit waar we naartoe moesten terwijl ik las dat, volgens de map die onze bereidwillige reisagente had samengesteld, we in Williams moesten zijn. Dat scheelt wel zo’n 95 km… Gelukkig tot de constatatie gekomen dat we desondanks toch goed zaten, oef!

*En mijn schat maar roepen ‘ga even langs de kant’. Gemakkelijk gezegd, met een canyon naast je auto…*

Eenmaal toegekomen in Yavapai Lodge en ingecheckt, hebben we gemerkt dat de kamer heel basic is. Nu ja, grote luxe zijn we niet gewoon op deze reis (behalve in Las Vegas) dus een propere kamer met bed en douche is voor ons al meer dan genoeg.

We waren beiden wat loom en hadden niet veel zin om ver te rijden om nog iets deftigs te vinden om te eten, dus zijn we maar naar de Cafetaria (ja, je leest het goed) te gaan om iets te eten. Je gaat nooit geloven wie (of wat, hangt er van af hoe je het bekijkt) we onderweg zijn tegengekomen: nee, niet Eddy Wally, ook niet La Esterella maar 2 reeën die aan het grazen waren achter een stel lodges. We konden tot op een tiental meter bij hen komen, ongelooflijk! Basic kamer, whatever, dit maakte al een heel stuk goed.

*Waren het bergleeuwen geweest, deze post was veeeel korter geweest…*

Het cafetaria-eten is vrij goed meegevallen: voor Lieven een Chicken pot pie (het zag er niet vree appetijtelijk uit ma het viel blijkbaar wel mee) en ik had veilig gekozen: spaghetti. Het was lekker maar ik moet toch zeggen, mama, die van jou is stukken beter ze! Toevallig geen zin om een pot spaghettisaus (zonder champignons) te maken deze week? ;-)

Hoe charmant de wandeling naar de cafetaria was, hoe creepy het was om terug te keren naar de kamer. Het was pikkedonker en we zagen met moeite elkaar. Nu moet je weten dat we eerder op de dag de ‘krant’ hadden gekregen van de Grand Canyon waarin de lokale flora en fauna verduidelijkt werd: ratelslangen, bighorn sheep, bergleeuwen… Begrijp je nu waarom wij opgelucht waren om een auto te zien of te merken dat een groep Franssprekende mensen achter ons liep met een zaklamp? Moraal van het verhaal: morgen onze koplamp (aka Petzel) meenemen als we hier in de buurt iets gaan eten.

*Note to self: grapjes over ratelslangen zijn op dit moment niet aangewezen… Nu weet ik wel niet wat er zo angstaanjagend is aan een bighorn sheep… ‘Honey, what’s that???’ … ‘Mèèèèèèèèhhh’ … ‘AAaaaagghhh’… don’t think so…*

Maar zoals je kunt merken, zijn we heelhuids op de kamer geraakt om onze blog te schrijven.

*ps: Voor de ongerust ouders onder ons: in Grand Canyon staan geen GSM masten... Er is hier dus ook geen ontvangst :).*
