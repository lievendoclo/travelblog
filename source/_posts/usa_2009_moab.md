title: Op naar Moab en de rest van Bryce...
date: 2009/05/24
categories:
- USA 2009
---

Vanmorgen opgestaan met een heerlijk zonnetje. Het was uiteraard een leuke afwisseling van gisteren, maar we durfden uiteraard niet te luid te juichen. Het weer kan hier snel omslaan.

De auto opnieuw volgeladen met de bagage, en op naar de plekken die we gisteren niet konden zien. Dus eerst naar Farview Point, een plaats die zijn naam echt niet gestolen heeft. Hier kan je echt  mijlenver zien, dichtbij de gekende Bryce rotsen, verder weg de heuvels van Utah. Dan terug naar de Natural Bridge. Op het bord staat echter dat Bridge niet klopt, maar dat het eigenlijk een Arch is. Not that we care, het is toch een prachtige zicht, zo'n enorm gat in een stuk massief rots.<!--more-->

Op de weg terug naar Inspiration Point stoppen er opeens enkele auto's voor ons. We keken even rond en ja hoor, twee antilopes twijfelden om de weg over te steken. We waren echter net te laat om een foto te kunnen trekken. Maar het was toch eens leuk om te zien, wilde beestjes. Op Inspiration Point opnieuw een geweldig uitzicht. Maar er kwamen weer enkele donkere wolkjes opzetten en we vreesden voor het ergste. Gelukkig de weergoden waren ons gunstig gezind en geen regen. Na Paria View hadden we zowat alles gezien wat de moeite te zien was en besloten we eindelijk op weg te gaan naar Moab. Opnieuw een rit van 275 mijl, dus niet niks. Deze keer ook niet vergeten om vol te tanken, we gaan het lot geen tweede keer tarten.

Onderweg gestopt aan onze vaste middagstop, Subway, maar hier zijn de broodjes echt onmenselijk duur. Ze maken gretig gebruik van hun monopolie en vragen ongeveer het dubbele van ergens anders. 22 dollar voor 2 broodjes... mensen, de Subway bij Bryce is een te mijden plek.

Niet veel belangrijks onderweg, maar wel 1 spijtig voorval: een chipmunk heeft zijn geluk op de proef gesteld door de highway over te steken. Hij is nog tussen mijn wielen geraakt, maar de jeep recht achter ons had hij niet verwacht en bijgevolg is hij nu rijstpap met gouden lepeltjes aan het eten. Even een moment stilte voor Knabbel...

We naderden Moab en een hoop donkere wolken begroeten ons. Oh help, niet opnieuw. Bliksemflitsen alom, maar wonder boven wonder, we bleven langs de zijkant van de storm. En ja hoor, een uurtje later, geen onweer meer aan de hemel te bespeuren, het werd zelfs vrij warm.

Ingecheckt in het hotel (weer 1e verdieping en geen lift...) en de kamer bekeken. Zoals gewoonlijk in orde, niks te luxueus, maar gewoon in orde, zoals we het willen. Twee queen beds, een aparte badkamer. Even gekeken in de gids wat er te bikken viel in de buurt (het hotel zit aan de rand van Moab, dus de auto gebruiken is nodig) en de auto ingesprongen.

Ons initieel idee was Zax, maar uiteindelijk zijn we geëindigd in Pizzahut. Kort gezegd, we hebben ons goedkoopst avondmaal tot nu toe gehad, en we hebben zelfs de rest die we niet opkregen meegekregen (gewoonte hier, dus ja).

Terug naar de kamer, spijtig enoeg heeft den deze hier al enkele onaangename bezoeken gehad aan het toilet (is nu een biohazard zone), het eten bij Ruby's begint precies toch zijn prijs te hebben. Ik ben enkel dankbaar dat mijn darmen tenminste gewacht hebben tot Moab...
