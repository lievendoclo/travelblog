title: De eerste dag
date: 2009/05/15
categories:
- USA 2009
---

Opgestaan rond 6u15… Veel te vroeg maar ik heb die nacht dan ook niet veel geslapen, in een poging om toch zo weinig mogelijk jetlag te hebben.

Farrah en Onno stonden om 8u15 aan de deur, en dankzij de afwezigheid van files op de Brusselse Ring waren we meer dan op tijd op de luchthaven. Op ons gemak ingecheckt, voorafgegaan door de eerste barrage van vragen à la ‘Heeft u uw koffers zelf ingepakt’. De befaamde Amerikaanse paranoia…

Onze vlucht is ook op tijd vertrokken – leve de lijnvlucht – en na 8 lange uren geland in Philadelphia (Philly voor de locals) om 15u30. En dan de beruchte Amerikaanse veiligheidsmaatregelen. Je begint met de immigratiedienst, waarbij vingerafdrukken en een foto (met webcam, het is crisis he mensen) worden genomen. En om een idee te geven: Philadelphia is een hub, en gewoon al het aantal kottekes waarin die mannen zitten is overweldigend. Farrah wou natuurlijk moeilijk doen en aan die kerel vragen waarom vingerafdrukken nodig waren. Ik heb haar dan maar gewezen op het bordje waarop stond dat onderbreken of het werk moeilijk maken van die kerels bestraft kon worden met 5000 dollar boete en 3 jaar gevangenisstraf. Aangezien ze hier nog zit, kan u reeds vermoeden dat ze dat plan heeft laten varen (vooraleer, eigen aan mijn schat, toch een klapke te maken met die gast).
<!--more-->

Dan deel twee: de douane. Standaard papiertje ingevuld op de vlucht, en gelukkig geen bagage check gekregen. We zien er dan ook niet zo terroristisch uit… Door een metaaldetector en scanner, zonder jas of schoenen en uiteindelijk… vrijheid. Te onthouden voor volgende keer: geen bottines aandoen, te veel werk.

Dan twee uur gewacht op de aansluiting naar San Francisco, die opnieuw mooi op tijd was. Het vertrek ging net iets minder vlot, er was een kleine file van een 10-tal vliegtuigen voor ons. Toch redelijk op tijd kunnen opstijgen…

Op het vliegtuig hebben we kennis gemaakt met een local (ook een geek, what a coincidence), die ons enkele leuke tips gegeven heeft. Farrah, het sociale beestje zoals iedereen ze kent, heeft natuurlijk tijdens de vlucht uitvoerig gebabbeld met die madam, over uiteenlopende zaken als Bush, de ziektezorg in de VS, de criminaliteit… je weet wel, de lichtere zaken der wereld. Zo hebben we ook kennisgemaakt met een van de nieuwigheden in Amerika – net op de markt – namelijk een pastilleke dat alle bacterien in je mond naar de eeuwige jachtvelden verwijst (een tandenborstel in een pilleke zeg maar). Wie een business opportunity in België zoekt, this is it!

Onder de verlichting van de avond in San Francisco geland. Deze keer geen controle, blijkbaar dien je dat maar eenmaal te ondergaan. Bij de bagage was het wel even wachten, maar uiteindelijk kwam de boel in actie. Mijn bagage kwam vrij snel, die van Farrah… die kwam wat later, tot grote ergernis van mijn schat uiteraard. Tijdens het wachten kwam de vrouw die ons gezelschap had gehouden op de vlucht ons nog een prettig verblijf werken en ons zomaar even 40 dollar geven voor het verblijf. Mijn mening over Amerikanen heeft nu al een ferme ommekeer gemaakt.

Met de taxi naar het hotel, naar de kamer en de bagage gedropt. We hadden beiden nog niet gegeten en dan maar iets in de buurt zoeken om te eten. We zijn geëindigd in Grubsteak, een soort van burgertent. Niet je alledaagse McDonalds, dat wel. De burger zijn hier op houtskool gebakken, en smaken wel een stuk beter. Wel even wennen aan de rekening, de prijzen op de kaart zijn zonder taks of fooi. Dus grofweg 20% bij de prijs rekenen en je zit erop. Nu, zelfs met die percenten erbij, nog steeds vrij schappelijk.

Terug naar de kamer en in bed gekropen. Het heeft niet lang geduurd of we waren beiden uitgeteld. Op naar de volgende dag…
