title: Grand Canyon... Grander...
date: 2009/05/28
categories:
- USA 2009
---

Aangezien het weer niet echt zo geweldig was gisteren, en we vandaag toch tijd over gaan hebben, zijn we vandaag maar begonnen aan de verkenning van Grand Canyon. En het is Grand. Niks kan de ervaring overstemmen dan op de rand (rim) van de Grand Canyon te staan. Maar daar gaan we het direct wel over hebben.

Onze ochtend... Een onbijt was aan de orde, we hadden beiden niet zo goed geslapen. De Grand Canyon is koud 's nachts, en dat zijn we niet meer gewoon. Het kan natuurlijk ook te maken hebben met het feit dat ik de lakens weer eens gestolen heb... Tjah, Farrah kan niet alles hebben in het leven he, ze heeft mij al!

*Het kan er natuurlijk ook aan gelegen hebben dat mijn lieve schat 2 ramen had opengezet, ze had vergeten dicht te doen en natuurlijk aan de kant van het raam sliep...*
*Daarenboven stond de fan (oftewel de ventilator aan het plafond) aan en als je dan inderdaad een 'schoeper' (iem. die steelt, nvdr) hebt naast je liggen dan gaat het volgens Lieven om het broederlijk delen principe: ik koud, iedereen koud. *

Dus deze morgen weer naar de refter voor het ontbijt. Ik zeg refter, want daar lijkt het echt op. Maar 't is goedkoop, snel en relatief lekker. Of het ook gezond ik zullen we wel merken later op de dag zeker?<!--more-->

Na mijn 'All American Breakfast' en Farrah haar flapjacks en worstjes (vanwaar haar obsessie voor dat laatste, ik weet het nog steeds niet...) op naar de bezienswaardigheid van deze streek: de Grand Canyon (had je niet zien aankomen he).

*Ik heb ook graag iets hartigs bij mijn ontbijt, mag het even?*
*En trouwens omelet met bacon en/of sausages met rozemarijnpatatten 's morgens, kweet nie ze... Nog voorstanders van dit soort ontbijt?*

Voor ons wilde dit dus zeggen: in de auto en terug naar het begin van het park, zo'n 28 mijl verder, aangezien daar het eerste viewpoint is. Onderweg opnieuw enkele herten gezien (4 deze keer), maar ik kon niet zo gemakkelijk stoppen als er uberhaupt geen berm is... Farrah heeft gelukkig haar zoomlens reeds paraat en dus kunnen er nog enkele leuke foto's gemaakt worden.

*Dat snap ik natuurlijk wel maar als een dierenvriend gelijk ik herten ziet én haar camera bij de hand heeft, dan hoop ik natuurlijk dat we zo vlug mogelijk kunnen stoppen. Je kan je wel voorstellen dat ik zo fier als een gieter was dat ik nog net op tijd de herten kon vastleggen op foto... \*glunder glunder\**

Eens aan ons beginpunt stappen we in de auto en ik kan je verzekeren, je mond valt open. Veel verhalen doen de ronde over de schoonheid van 'The Grand', maar je moet het zelf gezien hebben om het te geloven. Zover als je kan zien enkel kloof te bespeuren, in kleurfacetten die zelfs geen foto eer kunnen aandoen.

Maar zoals gezegd, het was niet ons enigste viewpoint. Namen die te moeilijk zijn om te onthouden (Indianennamen) maar die allen een andere schoonheid van de canyon ontluiken. Als een mens hier niet stil van wordt, ik weet het ook niet.

Na ongeveer elk punt op de kaart aangedaan te hebben (het zijn er veel) begonnen we toch stilaan weer wat honger te krijgen. Even naar de supermarkt achter brood en hete honden (de ketchup en mosterd hadden we nog) en we waren weer vertrokken naar de kamer voor wat te bikken. Deze keer niet zo veel gegeten, zowel de honden als het brood viel ferm tegen...

*Dat kan je wel zeggen ja, onze magen lieten ons direct weten wat ze ervan vonden.. niet voor herhaling vatbaar.*

We waren voorbereid om een zonsondergang te zien op de Grand Canyon, maar het weer gooide zoals gewoonlijk weer roet in het eten (het Weather Channel noemt mei echt freaky dit jaar). Onweer en bliksems obscuurden de zon en op een paar mooi gekleurde wolken was het een vrij teleurstellende zonsondergang. Bij ons volgend bezoek beter zekers?

*We komen in ieder geval terug om die 'wondermooie zonsondergang' in de Grand Canyon te bewonderen. Hoe vlugger hoe liever... Maar natuurlijk moet de rekening dat toelaten dus wordt het weer een tijdje sparen.*

Opnieuw ons avondeten in de cafetaria halen zagen we niet echt zitten, dus wij naar een van de restaurants van de andere hotels. Mensen, de Bright Angel Lodge is qua eten echt aan te raden. Veel beter eten dan Yavapai, en veel gezelliger (nu ja, moeilijk was dat niet). Farrah gegaan voor de Trailblazer Fajitas, ik voor een lekker steak. Oh ja, Amerikaanse bakwijzen voor beginners:

- bleu froid: rare
- bleu chaud: medium rare
- saignant: medium rare (ge moet chance hebben, anders ist ne bleu chaud)
- a point: medium of medium well done (opnieuw, chance)
- bien cuit: well done (en da's echt doorbakken)
- detruit: nicely well done... (kettingzaag nodig)

Ik ga dus meestal voor de medium rare... Het laatse wordt echt vaak besteld hier.

*Om die reden had ik dus wijselijk besloten om iets te kiezen van de kaart dat ze naar mijn idee moeilijk konden verbrodden. En ik moet toegeven, de tortillas met gemarineerde stukjes gebakken biefstuk, vergezeld van gebakken uitjes en paprika's en de heerlijke salsa waren geen teleurstelling. Op de 'refried beans' (don't ask) en de zure room na heb ik braafjes mijn bord leeggegeten. \*yummie\**

Terug naar het hotel was wel weer een avontuur. Geen verlichting, echt leuk. Farrah aan het stuur, nog leuker ;)... Ene keer verkeerd gereden (ok, mijn fout, links en rechts verwisseld) maar toch uiteindelijk op ons kamer geraakt.

*Wat? Ik ben gewoon om met een automatiek te rijden, ik rijd graag met de auto maar ik heb vanavond wel gemerkt waarom ik heel de reis navigator ben geweest... Links en rechts verwisselen, oost en west switchen... toch wel cruciaal als je het mij vraagt als je ergens wil geraken. Maar we zijn er geraakt, dus waarom zouden we vitten?*

Net op tijd, iets later begon het te regenen (en is die nacht omgevormd tot onweer). Deze keer geen wilde beesten tegengekomen (of toch...).

*Ja, Lieven als hij begint te snurken... ;-)*

Tot morgen, beste lezertjes en lezerinnetjes.
