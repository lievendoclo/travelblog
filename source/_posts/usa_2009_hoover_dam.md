title: Hoover Dam en The Strip
date: 2009/05/21
categories:
- USA 2009
---

Vandaag een drukke dag gehad. Geen ontbijt in het hotel (veel te duur, het buffet gisteren was ook  nogal aan de prijzige kant, dus we gaan even budget). Vandaag stond Hoover Dam op het menu, dus wij de auto in, onderweg zouden we wel iets vinden voor te ontbijten.

Niet dus. Tis te zeggen, toch niet waar wij gereden hebben. Dus wij twee naar Wal Mart, nog zo'n bastion van de Amerikaanse comsumptiemaatschappij. 't Is de Colruyt, maar dan supersize, zoals alles hier. Ze hebben hier zelfs een greeter die iedereen hallo zegt die binnenkomt. Economische crisis... sure. Uiteindelijk een doos botercake, melk en een fles Starbucks frappucino mee. Toch wel lekker. By the way, wie een IPod nano zoekt, hij is hier 25 euro goedkoper.<!--more-->

Uiteindelijk bij Hoover Dam geraakt. Parking moet je hier wel betalen, maar 7 dollar... das peanuts. Naar het Visitor's Center gewandeld (ook hier is het pokkeheet), maar dan moet je door de security, maar ja hoor, den dezen had het weer zitten. Wij hebben altijd bestek mee. Mag dus niet, dus ik weer in de verzengende hitte naar de auto, en alles wat mogelijks een probleem is in de koffer gesmeten en terug. Weeral prijs: ik had blikjes mee en zogezegd fruit (twas eigenlijk mijn nieuwe stressball die ik in Staples had gekocht). Ik zag het dus echt niet zitten om opnieuw naar de auto te trotten, dus dan maar die blikjes ter plekke opgedronken. De derde keer dat mijn zak door de scanner ging heeft die gast niks meer durven zeggen, tis maar dat je een gedacht had hoe kwaad ik op dat moment was.

Gekozen voor de 11 dollar (de andere was 30 dollar, en daarmee ging je tot in de dam) toer, begint met een presentatie, dan ga je met de lift naar beneden naar de powerplant, en terug naar boven naar de tentoonstelling. Vrij indrukwekkend, zeker als je weet dat ze dit in 4 jaar gedaan hebben. In België hebben ze verdorie 6 maand nodig voor een stukje E17 van 1 km te asfalteren... Ze hebben die dam zelfs 2 jaar te vroeg en onder het budget klaargekregen. Ik denk niet dat dit vandaag nog zou lukken, maar het was toen dan ook de Grote Depressie.

Op weg naar buiten krijg je een zicht op het nieuwste project hier: een boogbrug over de Colorado, die bedoelt is om verkeer weg te krijgen van Hoover Dam. Het wordt de 5e grootste in de wereld. Voorziene datum: binnen 2 jaar. Over de dam gewandeld en terug (dus naar Arizona en terug, want de overkant van de dam is Arizona, de andere Nevada). Opnieuw, veel te warm, mijn koppeke begon weer te koken.

Het volgende op de lijst was de Strip, maar die wilden we zien 's avonds, zonder neon is Las Vegas niet zo plezant. Dus wat gerust op de kamer en douchke genomen. Iets voor 21u vertrokken met de auto, te voet van de Rio naar de Strip is toch net iets te ver. Geparkeerd op de Bellagio parking en op naar de Strip. Je ziet direct dat de Bellagio een prijsklasse hoger ligt. Marmer, schone lusters, ... Niet mis. We waren net op tijd om het waterspektakel voor de Bellagio te zien. Honderden fonteinen die dansen op Con Te Partiro van Bocelli. Prachtig om te zien. Verder op de Strip de uitbarstende vulkaan van The Mirage. Alles is hier over-the-top, voor ons tweetjes net iets té.

Door Caesar's Place gewandeld, en daar ook iets gegeten. Ook niet goedkoop, maar we beginnen te beseffen dat dit moeilijk wordt in Las Vegas. Nu ja, twas wel lekker. Caesar's is zo mogelijk nog meer pompeus ingericht dan the Bellagio. Romeinse standbeelden, marmeren vloeren, ... Man man, kan het nog erger? En natuurlijk overal slotmachines te zien (zelfs op straat!). Aan de overkant zie je dan het Paris hotel. Niet, niet van dat blond leeghoofd, maar een 1/2 replica van de Eiffel toren... Het kan dus erger.

Onze wandeling op de Strip geëindigd aan The Venetian. Dit is hét tophotel en casino in Las Vegas en dat zal je geweten hebben. De voorkant van het hotel is een replica van het San Marco plein, binnenin een replica van de Venetiaanse kanalen (met gondels). Met een dak dat je laat denken dat het dag is. Oh ja, vergeten te zeggen... Dit is op het 1e verdiep. Op het gelijkvloers zit het casino! Niet te geloven gewoon.

Maar Las Vegas begint ons beiden te vermoeien. Het is gewoon te veel. Misschien ligt het aan ons (er zijn ook mensen die bijvoorbeeld een vakantie in Blankenberge met de mobilhome leuk vinden), maar dit is gewoon vermoeiend. Overal slotmachines, langs de weg 200 meter mensen die je kaartjes geven voor dat ander soort genot (een kerel naast ons is door die rij geraakt en had op het einde een stapeltje van 5 cm :), die daarna direct in de vuilbak geraakt is). En dat herhaal je zo'n 3 a 4 keer. De eerste keer is het leuk, daarna wordt het gewoon vervelend.

Terug naar het hotel en ons bed in. We zijn allebei blij dat we Vegas gezien hebben, maar 1 nacht was wel genoeg geweest. Maar dan wordt het wel krap als je ook Hoover Dam wil gezien hebben... Nu ja, morgen zitten we weer in de natuur.
