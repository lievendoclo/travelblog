title: Op naar Monument Valley
date: 2009/05/26
categories:
- USA 2009
---

Vanmorgen eindelijk eens opgestaan door de wekker. Maak dat mee. Raken we nu eindelijk gewoon aan het tijdsverschil, moeten we bijna weer naar huis. Maar dat zijn nu eens zorgen voor later.

Deze morgen weer naar Denny’s voor het ontbijt (tis snel en goedkoop). Deze keer gene Grand Slam voor mijn schat, maar de gewone toast en worstjes. Opnieuw de ober die vraagt of ze geen Grand Slam ontbijt moet hebben… Nee dus. Ik daarentegen heb er mij toch weer aan gewaagd doch iets gezonder: met een glaasje fruitsap :).

Vandaag verlaten we Moab weer, zoals we ondertussen al gewoon worden. Je wordt er eigenlijk raar genoeg wel aan gewoon, zo verblijfjes van enkele dagen. Je moet ook uiteraard niet te lang op een plek blijven, de VS is veel te groot en te mooi daarvoor.<!--more-->

Vandaar staat de rit naar Kayenta in Navajo gebied op het menu. Maar omdat we daar maar 1 nacht blijven moeten we wel zoveel mogelijk gezien hebben onder de weg. Dus ondertussen even stoppen in de Canyonlands. Nu ja, even stoppen… het is een omweg van zo’n 80 mijl :S. Onderweg vind je wel opnieuw ongelofelijk mooie landschappen. Eerst passeer je langs Newspaper Rock, een stuk rots met enkele tekeningen op (petrogliefen noemt dat blijkbaar). Niemand weet echter wat er op staat, dus voor het zelfde geld staat het recept voor armageddon op die rots (of, hoe word ik mager in 1 dag…).

Dan passeer je weer langs de ingang van het park, waar we opnieuw ons kaartje afgeven. Even inscannen en je bent weer verder. Was alles in het leven maar zo gemakkelijk. Het begint zo ondertussen een automatisme te worden: ik rij naar het hokje, Farrah zoekt achter de kaart en paspoort en zo gaat het lekker vlot. Teamwork mensen… gewoon puur teamwork, I tell ya.

Canyonlands is zo’n beetje een samenvatting van andere parken. Niets groots, gewoon mooie rotsformaties, flora en hier en daar wat fauna onder de vorm van hagedissen. Onze Cruiser heeft wel alweer eens zichzelf mogen bewijzen op een onverharde weg, maar wonder bij wonder, hij doorstaat het erg goed :).

Vanuit Canyonlands richting Kayenta. Onze route zou ons normaal gezien via twee snelwegen rijden, maar ik wou per se Valley of the Gods zien (iemand die ooit Airwolf heeft gezien, weet waarom). Dus even een omweg (niet zoveel, denk iets van een 10 mijl). Als een added bonus: onze route ging ook langs een Scenic Byway. Als ik iemand een raad moet geven: als je kan, neem deze. Je gaat er geen spijt van hebben. En al zeker niet van deze. Dit is zo een van die wegen waar je uit volle borst ‘Kijk uit waar je gaat, kalf!’ kan roepen. Niet omwille van de slechte chauffeurs… Nee, het loopt hier vol van de koeien en kalveren. ’t Is een Open Range weg, wat wil zeggen dat je ten allen tijde bewust moet zijn van overstekend vee. Niet veel erger dan een rit door Brussel, lijkt me. Voordat je de weg opgaat kom je trouwens een waarschuwing tegen voor onverharde, steile hellingen. Leuk. Nu ja, na 40 mijl geen onverhard gezien te hebben en niks anders dan plat, dachten we dat het bord verouderd was.

Oh nee hoor. Opeens verandert de weg in een kiezelbaantje, zigzaggend naar beneden met naast je een afgrond van tientallen meters (heb hier Farrah eventjes de stuipen op het lijf gejaagd door, op een berekende weg, even wat sneller te rijden… fun eigenlijk :)). Spijtig genoeg roept de natuur soms op de meest onmogelijke momenten en dat heeft ons Farrah mogen merken. Daarmee hebben we weer een extra pluspunt ontdekt aan de PT Cruiser: de wijde deuren. De rest van het verhaal dient u maar persoonlijk aan haar te vragen.

Eens beneden kom je aan de Valley of the Gods: een uitgestrekte woestenij met majestueuze rotsen die de horizon verbreken. Je wordt er wel even stil van. Met een beetje verbeelding zie je de beroemde helicopter overvliegen. De weg door de Valley hebben we niet gedaan: het was ook onverhard en we wilden onze PT Cruiser nu ook niet te zwaar molesteren.

Op weg naar Kayenta kom je Monument Valley tegen. Het was echter al laat, dus we besloten door te rijden en dit morgen wel te doen. Opnieuw, naast deze weg is het vergapen troef. Ongelofelijk vergezichten, die nog eens worden benadrukt door de ondergaande zon. Onder de weg nog eens goed verschoten van vee op de weg… Gelukkig had Farrah me op tijd gewaarschuwd.

Eens in Kayenta wat verwarring: onze kaart was opeens niet meer zo duidelijk. Dus wij maar op goed geluk gereden en gelukkig de juiste keuzes gemaakt. Het hotel was opnieuw goed in orde, internet op de kamer (maar viel uit, waardoor deze post toch wat later is). Naast het hotel: de Burger King. We hadden nu al bijna 2 weken weerstaan aan de Amerikaanse ‘way of life’, het was genoeg geweest. De Whoppers smaakten desgevallend enorm goed!

Met een verzadigd gevoel in bed gekropen (voor de eerste keer apart, de bedden waren echt te klein). Tot morgen, zou ik zo zeggen.
