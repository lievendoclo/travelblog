title: Bryce Canyon en over rooie steentjes
date: 2009/05/22
categories:
- USA 2009
---

Met een opgelucht gevoel laten we Las Vegas achter ons. Op naar de highway en naar Bryce Canyon. Het was 171 mijl highway vandaag, dus een vrij saaie rit. Maar het zicht was wel leuk. Langs China Lake gepasseerd, je weet wel, die plek waar het US Army paddenstoelwolkjes gekweekt heeft en waar een of andere idioot een zilveren deksel uit de ruimte heeft laten vallen met nog wat voedselresidu's op (dit is allemaal top secret en de voedselanalyse gaat tot op de dag van vandaag nog steeds door in Area 51. De resultaten worden doorgegeven aan McDonalds uiteraard).<!--more-->

Onderweg gestopt voor te tanken (ene keer, maar geen twee keer) en een nieuwe voorraad ijs voor ons frigoboxen. De Subway broodjes worden zo onderhand al onze vaste on-the-road maaltijd, maar das nie erg.

Op weg naar Utah moet je door de Virgin River Gorge, een bochtig stuk op de highway met een adembenemend zicht! Eens daardoor zit je in Utah, een heel wat groenere staat dan Arizona of Nevada. Een andere tijdszone ook trouwens.

De weg naar Bryce Canyon is zonder meer prachtig te noemen. Je gaat langs een Scenic Byway en de weg doet zijn naam alle eer aan. Langs berkenbossen, heuvellands, eindeloze vergezichten, ... En dan rijd je Red Canyon binnen.

Red Canyon kan je best omschrijven als surreëel, iets wat gestolen blijkt te zijn van een Marslandschap. De weg baant zich langs oranjerode hillsides en je weet echt niet waar te kijken. Na een 10-tal minuutjes ben je erdoor gereden, maar reken toch maar op een 30 minuten als je echt wil genieten van het landschap.

Uiteindelijk aangekomen in onze eindbestemming voor vandaag: Ruby's Inn. Het grootste hotel hier in omstreken en toch wel een goed hotel. Bijna iedereen die naar Bryce gaat zit hier. Aan de overkant van de straat vind je enkele winkeltjes, waaronder een die mineralen en stenen verkoopt. Een van de zaken die je hier onder andere kunt kopen zijn volledige, ongeopende geodes, die ze voor jou ter plekke opensnijden. Aangezien ik weet dat Farrah dit graag ziet, heb ik haar eentje cadeau gedaan. Nadat de steen is doorgesneden (met een diamantzaag, gene gewonen BlackNDekker hoor), heeft Farrah als eerste de inhoud kunnen bekijken: prachtige blauwe kristallen. Prijs van dit alles: 12 dollar. Belachelijk laag, als je je indenkt hoeveel dit kost als je dit in België zou kopen.

Vanavond op het menu: een buffet in Ruby's Inn. Ik weet niet waar die gasten van de Rough Guide het halen (ze noemen het dreadful), want het eten was superlekker. Drank moet je 1 keer betalen, refills zijn gratis en je mag zoveel keer naar het buffet gaan als je wilt. Granted, veel keuze is er niet, maar wat ze hebben is overheerlijk. Een absolute aanrader, zeker voor die prijs, 18 dollar!

Terug naar de kamer, waar ik op dit eigenste moment de zin typ die u net gelezen heeft. Morgen Bryce Canyon verkennen.
